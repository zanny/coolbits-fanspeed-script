#! /usr/bin/env python3
""" 
Coolbits based fan speed control script!

This script is meant to be easy to configure to set fan speeds on any coolbits supporting Nvidia GPU.
To enable coolbits, edit your /etc/X11/xorg.conf and under section "Device" add the line:
Option "Coolbits" "4"
Don't forget to edit it as root and save it!

To use overclocking, use Option "Coolbits" "5" instead, and set clocks= the core and memory speeds you want.
Warning! : if you don't set clocks=false with coolbits=4, the script won't work because it expects to overclock
without running in a state where it is enabled. You must change clocks to clocks=false to use only fan controls.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses.
"""

# Speeds are temp : speed key / values. Order doesn't matter.  
# Non-integer temps won't be used and non-integer speeds will be casted.
# Temps must be between [0,105), speeds between [10,100].  Lower bound unmentioned temps are defaulted to min speed.
fanspeeds = {30 : 30,
             40 : 40,
             50 : 50,
             60 : 60,
             65 : 70,
             70 : 80,
             75 : 90,
             80 : 100}
clocks = (600, 1200)
sleepSecs = 5
tempRange = 105
minSpeed = 10

# One problem with this configuration is that some cards have thermal ceilings above 105c.  
# Also, a card running under freezing point would cause problems.  If you have a card running
# hotter than 105c without a thermal shutdown, you should probably worry about the plastic fracturing.

from subprocess import check_output, check_call
from re import compile
from time import sleep

speedmap = []
speed = minSpeed
for temp in range(tempRange):
  if temp in fanspeeds:
    speed = int(fanspeeds[temp])
  speedmap.append(speed)

tempRegex = compile("\): (\d{1,3})\.\n")

def getTemp():
  output = check_output(["nvidia-settings", "-q", "gpucoretemp"], universal_newlines=True)
  return int(tempRegex.search(output).group(1))

def setSpeed(speed):
  # reset fan control state each update to make sure it never gets switched off outside execution
  check_call(["nvidia-settings", "-a", "[gpu:0]/GPUFanControlState=1", "-a", "[fan:0]/GPUCurrentFanSpeed={}".format(speed)])

def main():
  try:
    # reclocking
    if clocks:
      check_call(["nvidia-settings", "-a", "[gpu:0]/GPUOverclockingState=1", "-a", "[gpu:0]/GPU3DClockFreqs={},{}".format(clocks[0], clocks[1])])
    # due to the nondeterministic run time of nvidia-settings polls, we only check temps and update only when boundaries are crossed
    speed = speedmap[getTemp()]
    setSpeed(speed)
  
    while True:
      newSpeed = speedmap[getTemp()]
      if newSpeed != speed:
        speed = newSpeed
        setSpeed(speed)
        sleep(sleepSecs)
  
  finally:
    if clocks:
      check_call(["nvidia-settings", "-a", "[gpu:0]/GPUOverclockingState=0"])
    # when the process terminates, we want to revoke manual fan control.
    check_call(["nvidia-settings", "-a", "[gpu:0]/GPUFanControlState=0"])

if __name__ == '__main__':
  main()